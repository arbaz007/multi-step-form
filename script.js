let inputData = document.getElementsByClassName("user-data");
let form = document.querySelector(".form");

let error = false;

function errorMsg(msg, spanNode, node) {
  spanNode.innerHTML = msg;
  node.style.border = "1px solid hsl(354, 84%, 57%)";
  console.log("error in errorMsg", error);
}

function removeError(errorNode) {
  errorNode.innerHTML = "";
  error = false;
  errorNode.previousElementSibling.style.border =
    "2px solid hsl(228, 100%, 84%)";
}

let [username, email, phone] = inputData;
console.log(username);
console.log(email);
console.log(phone);
username.addEventListener("input", () => {
  removeError(username.nextElementSibling);
});

email.addEventListener("input", () => {
  removeError(email.nextElementSibling);
});

phone.addEventListener("input", () => {
  removeError(phone.nextElementSibling);
});

function getFormData() {
  if (username.value == "") {
    error = true;
    error
      ? errorMsg(
          "This feild is required",
          username.nextElementSibling,
          username
        )
      : "";
  }
  if (email.value == "") {
    error = true;
    error
      ? errorMsg("This feild is required", email.nextElementSibling, email)
      : "";
  }
  if (phone.value == "") {
    error = true;
    error
      ? errorMsg("This feild is required", phone.nextElementSibling, phone)
      : "";
  }
  let emailRegex = /^[^\s@]+@[^\s@]+\.[^\s@]+$/;
  if (email.value != "") {
    if (!emailRegex.test(email.value)) {
      error = true;
      error
        ? errorMsg(
            "please provide valid email",
            email.nextElementSibling,
            email
          )
        : "";
    }
  }
  let phoneRegex = /^\d{10}$/;
  if (phone.value != "") {
    if (!phoneRegex.test(phone.value)) {
      error = true;
      error
        ? errorMsg(
            "please provide valid phone number",
            phone.nextElementSibling,
            phone
          )
        : "";
    }
  }
  return error;
}

let next = document.getElementById("next");
let iterateActive = (countDiv, classValue, direction = 0) => {
  let index, flag;
  flag = getFormData()
  for (let i = 0; i < countDiv.length; i++) {
    if (countDiv[i].classList.contains(classValue)) {
      index = i;
    }
  }
  
  if (!flag) {
    if (direction == 0) {
      if (index + 1 == countDiv.length) {
        countDiv[0].classList.add(classValue);
        countDiv[index].classList.remove(classValue);
      } else {
        countDiv[index].classList.remove(classValue);
        countDiv[index + 1].classList.add(classValue);
      }
      if (index + 1 > 0) {
        prev.classList.remove("none")
      }
      if (index + 1 == 4) {
        confirmbtn.classList.add("btn-show")
        next.classList.remove("btn-show")
      }
    }
    if (direction == 1) {
      if (index == 0) {
        countDiv[countDiv.length - 1].classList.add(classValue);
        countDiv[index].classList.remove(classValue);
      } else {
        countDiv[index].classList.remove(classValue);
        countDiv[index - 1].classList.add(classValue);
      }
      if (index - 1 == 0) {
        prev.classList.add("none")
      }
      index -= 1
      
    }
    if (index == 2) {
      confirmbtn.classList.add("btn-show");
      next.classList.remove("btn-show");
    } else {
      confirmbtn.classList.remove("btn-show");
      next.classList.add("btn-show");
    }
  }
};

function togglePlan() {
  let input = document.querySelectorAll("input[type='radio']");
  let inputYearlyValues = ["90", "120", "150"];
  let inputMonthlyValues = ["9", "12", "15"];
  let labelYearlyValue = ["$90/year", "$120/year", "$150/year"];
  let labelMonthlyValue = ["$9/mo", "$12/mo", "$15/mo"];
  let addOnsYearlyValues = ["10", "20", "20"];
  let addOnsMonthlyValues = ["1", "2", "2"];
  let freeTrial = document.querySelectorAll(".free-trial")

  if (planCheckbox.checked) {
    monthly.classList.remove("toggle-plan-active");
    yearly.classList.add("toggle-plan-active");
    planType = yearly.textContent;
    costPerType = "yr";
    freeTrial.forEach (each => each.classList.remove("none"))
  } else {
    monthly.classList.add("toggle-plan-active");
    yearly.classList.remove("toggle-plan-active");
    planType = monthly.textContent;
    costPerType = "mo";
    freeTrial.forEach(each => each.classList.add("none"))
  }

  for (let label = 0; label < labels.length; label++) {
    if (planCheckbox.checked) {
      labels[label].children[2].textContent = labelYearlyValue[label];
    } else {
      labels[label].children[2].textContent = labelMonthlyValue[label];
    }
  }
  for (let i = 0; i < addOnsPrice.length; i++) {
    if (planCheckbox.checked) {
      addOnsPrice[i].innerHTML = `+$${addOnsYearlyValues[i]}/Year`;
      addOnsValue[i].value = addOnsYearlyValues[i];
      addOnsValue[i].checked = false;
      while (addOnsSummaryContainer.firstChild) {
        addOnsSummaryContainer.removeChild(addOnsSummaryContainer.firstChild);
      }
      addOnsArray = [];
    } else {
      addOnsPrice[i].innerHTML = `+$${addOnsMonthlyValues[i]}/mo`;
      addOnsValue[i].value = addOnsMonthlyValues[i];
      addOnsValue[i].checked = false;
      while (addOnsSummaryContainer.firstChild) {
        addOnsSummaryContainer.removeChild(addOnsSummaryContainer.firstChild);
      }
      addOnsArray = [];
    }
  }
  input.forEach((each, index) => {
    if (planCheckbox.checked) {
      each.value = inputYearlyValues[index];
    } else {
      each.value = inputMonthlyValues[index];
    }
  });
}

function selectAddOns(each) {
  each.addEventListener("change", (e) => {
    let obj = {};
    let label = e.target.nextElementSibling;
    if (e.target.checked) {
      obj["addOnsContent"] = label.children[0].children[0].textContent;
      obj["addOnsValue"] = label.children[1].textContent;
      addOnsArray.push(obj);
      totalFares += +e.target.value;
      console.log(addOnsArray);
      createSummaryDetails(obj);
    }
    if (!e.target.checked) {
      addOnsArray = addOnsArray.filter(
        (item) =>
          item.addOnsContent != label.children[0].children[0].textContent
      );
      totalFares -= +e.target.value;
      console.log(addOnsArray);
      let childAddOnsDiv = addOnsSummaryContainer.children;
      for (
        let index = 0;
        index < addOnsSummaryContainer.children.length;
        index++
      ) {
        if (
          childAddOnsDiv[index].children[0].textContent ==
          label.children[0].children[0].textContent
        ) {
          addOnsSummaryContainer.removeChild(childAddOnsDiv[index]);
        }
      }
      totalSummary.innerHTML = `<p>Total: </p><p>$${totalFares}/${costPerType}</p>`;
    }
  });
}
function backToPlan() {
  addOnsArray = [];
  change = true
  while (addOnsSummaryContainer.firstChild) {
    addOnsSummaryContainer.removeChild(addOnsSummaryContainer.firstChild);
  }
  for (let i = 0; i < addOnsValue.length; i++) {
    addOnsValue[i].checked = false;
  }
  for (let i = 0; i < stepMain.length; i++) {
    stepMain[i].classList.remove("step-active");
  }
  stepMain[1].classList.add("step-active");

  for (let i = 0; i < step.length; i++) {
    step[i].children[0].classList.remove("count-active")
  }
  step[1].children[0].classList.add("count-active")
  next.classList.add("btn-show");
  confirmbtn.classList.remove("btn-show");
}

function createSummaryDetails(args) {
  let div = document.createElement("div");
  let p1 = document.createElement("p");
  let p2 = document.createElement("p");
  if (args.hasOwnProperty("planText")) {
    planSummaryContainer.innerHTML = `<span onclick='backToPlan()'>change</span><p>${args.planText}(${planType})</p><p>${args.planPrice}</p>`;
  }
  if (args.hasOwnProperty("addOnsContent")) {
    p1.innerText = args.addOnsContent;
    p2.innerText = args.addOnsValue;
    div.append(p1, p2);
    addOnsSummaryContainer.appendChild(div);
  }
  totalSummary.innerHTML = `<p>Total: </p><p>$${totalFares}/${costPerType}</p>`;
}
let change = false;
let totalFares = 0;
let planType = "Monthly";
let costPerType = "mo";
let step = document.getElementsByClassName("step");
let countDiv = document.getElementsByClassName("count");
let prev = document.getElementById("prev");
let stepMain = document.getElementsByClassName("step-main");
let plans = document.querySelectorAll("input[type='radio']");
let planCheckbox = document.querySelector("input[type='checkbox']");
let planvalue;
let addOnsArray = [];
let addOnsValue = document.querySelectorAll("input[name='addOns']");
let planObj = {};
let labels = document.getElementsByClassName("plan-label");
let planSummaryContainer = document.getElementById("plan-summary-container");
let addOnsSummaryContainer = document.getElementById(
  "addOns-summary-container"
);
let totalSummary = document.querySelector(".total-summary");
let addOnsPrice = document.getElementsByClassName("addOns-price");
let monthly = document.querySelector(".monthly");
let yearly = document.querySelector(".yearly");
let confirmbtn = document.querySelector(".confirm");

prev.addEventListener("click", () => {
  iterateActive(countDiv, "count-active", 1);
  iterateActive(stepMain, "step-active", 1);
});

next.addEventListener("click", () => {
  iterateActive(countDiv, "count-active");
  iterateActive(stepMain, "step-active");
});

plans.forEach((each, index) => {
  each.addEventListener("click", (e) => {
    planObj["planText"] = labels[index].children[1].textContent;
    planObj["planPrice"] = labels[index].children[2].textContent;
    planvalue = +e.target.value;
    totalFares = planvalue;
    createSummaryDetails(planObj);
  });
});

planCheckbox.addEventListener("change", togglePlan);

addOnsValue.forEach((each) => {
  selectAddOns(each);
});

confirmbtn.addEventListener("click", () => {
  iterateActive(stepMain, "step-active");
  confirmbtn.parentElement.style.display = "none"
});
